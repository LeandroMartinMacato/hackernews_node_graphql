const { ApolloServer } = require("apollo-server");
const { PrismaClient } = require("@prisma/client");
const fs = require("fs");
const path = require("path");

const { getUserId } = require("./utils");

const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const User = require('./resolvers/User')
const Link = require('./resolvers/Link')
const Subscription = require('./resolvers/Subscriptions')
const Vote = require('./resolvers/Vote')

const { PubSub } = require('apollo-server');

const resolvers = {
	Query,
	Mutation,
	User,
	Link,
	Subscription,
	Vote,
};

/* ------------------------------ Old resolvers ----------------------------- */
// const resolvers = {
//     Query: {
//         info: () => `This is the API of a Hackernews Clone`,
//         feed: async (parent , args , context) => {
//             return context.prisma.link.findMany()
//         }
//     },

//     Mutation:{
//         post:(parent , args , context , info) => {
//             const newLink = context.prisma.link.create({
//                 data: {
//                     url: args.url,
//                     description: args.description,
//                 },
//             })
//             return newLink
//         },

//         updateLink: (parent , args) => {
//             console.log("Updating!!")
//             console.log(args);

//             // update Link with DB

//         },

//         deleteLink: (parent, args) => {
//             console.log("Deleting!")
//             console.log(args);

//             // delete link in DB
//         },
//     }
// }

//initialize PrismaClient
const prisma = new PrismaClient();

const pubsub = new PubSub();

// ApolloServer , it accepts both the schema and resolvers
const server = new ApolloServer({
	typeDefs: fs.readFileSync(path.join(__dirname, "schema.graphql"), "utf8"),
	resolvers,
	context: ({ req }) => {
		return {
			...req,
			prisma,
			pubsub,
			userId: req && req.headers.authorization ? getUserId(req) : null,
		};
	},
});

// tell the server to listen to the port , then log in the terminal the url
server.listen().then(({ url }) => {
	console.log(`🚀 Server ready at ${url}`);
	console.log();
});
