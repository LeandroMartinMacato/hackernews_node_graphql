const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { APP_SECRET, getUserId } = require("../utils");

async function signup(parent, args, context, info) {
	console.log("signing up...");
	// hash the password using bcrypt
	const password = await bcrypt.hash(args.password, 10);

	// create user in the db
	const user = await context.prisma.user.create({
		data: { ...args, password },
	});

	// sign json web token
	const token = jwt.sign(
		{
			userID: user.id,
		},
		APP_SECRET
	);

	return {
		token,
		user,
	};
}

async function login(parent, args, context, info) {
	// find user in db
	console.log("logging in...");
	const user = await context.prisma.user.findUnique({
		where: {
			email: args.email,
		},
	});

	// user doesnt exist
	if (!user) {
		throw new Error("No such user found");
	}

	// compare user password against db
	const valid = await bcrypt.compare(args.password, user.password);

	// password invalid
	if (!valid) {
		throw new Error("Invalid password");
	}

	// Create token
	const token = jwt.sign({ userId: user.id }, APP_SECRET);

	return {
		token,
		user,
	};
}

async function post(parent, args, context, info) {
	const { userId } = context;

	const newLink =  await context.prisma.link.create({
		data: {
			url: args.url,
			description: args.description,
			postedBy: { connect: { id: userId } },
		},
	});

	// tell pubsub(subscription) of the new Link
	context.pubsub.publish("NEW_LINK" , newLink)

	return newLink;
}

async function vote(parent , args, context, info){
	// validate user first
	const userId = context.userId;

	// check if voter already voted
	const vote = await context.prisma.vote.findUnique({
		where: {
			linkId_userId: {
				linkId : Number(args.linkId),
				userId: userId
			}
		}
	})

	if(Boolean(vote)){
		throw new Error(`Already voted for link: ${args.linkId}`)
	}

	// Create a new vote connect to User and Link
	const newVote = context.prisma.vote.create({
		data: {
			user: { connect: { id: userId } },
			link: { connect: { id: Number(args.linkId) } },
		}
	})
	context.pubsub.publish("NEW_VOTE" , newVote)

	return newVote
}

module.exports = {
	signup,
	login,
	post,
	vote,
};
