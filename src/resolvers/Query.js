async function feed(parent , args , context){
    console.log("Getting Feed...");

    const where = args.filter
        ? {
            OR : [
                { description: { contains: args.filter } },
                { url: { contains: args.filter } },
            ],
        }
        : {};

    const links = await context.prisma.link.findMany({
        where,
        skip: args.skip, // get skip and take values from the args
        take: args.take,
        orderBy: args.orderBy,
    })

    const count = await context.prisma.link.count({ where })

    return {
        links,
        count,
    }

    // old
    // return context.prisma.link.findMany()
}

// discontinued
// function users(parent , args , context){
//     console.log("Getting Users...");
//     return context.prisma.link.findMany()
// }

function links(parent , args , context){
    console.log("Getting Links...");
    return context.prisma.link.findMany()
}

module.exports = {
    feed,
    links
    // users,
}