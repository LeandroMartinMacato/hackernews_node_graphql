const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

async function main() {

    //Create
    const newLink = await prisma.link.create({
        data: {
            description: 'I am the second item!',
            url: 'www.second.com',
        },
    })

    //find
    const allLinks = await prisma.link.findMany();
    console.log(allLinks);
}

main()
    .catch( e => {
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    })